all-cuda: 1_sum/reduce-cuda 2_stat/reduce-cuda
all-hip: hipify-commons 1_sum/reduce-hip 2_stat/reduce-hip

CUDA_VERSION=11.4

FLAGS=-I common -O3 -std=c++14
OMP_FLAGS=-fopenmp
OMP_OFFLOAD_FLAGS=-foffload=-latomic
OMP_OFFLOAD_FLAGS_CLANG=-fopenmp-targets=amdgcn-amd-amdhsa -Xopenmp-target=amdgcn-amd-amdhsa -march=gfx906

%-cuda.o: %.cu %.cuh
	nvcc -o $@ -c $< $(FLAGS)

%-cuda: %.cc %-cuda.o
	g++ -o $@ $^ $(FLAGS) $(OPT) `pkg-config --libs cuda-$(CUDA_VERSION) cudart-$(CUDA_VERSION)` $(OMP_FLAGS) $(OMP_OFFLOAD_FLAGS)

%.hip.hpp: %.cuh
	hipify-perl $< > $@

%.hip: %.cu
	hipify-perl $< > $@

hipify-commons: common/helpers-cuda.hip.hpp

%-hip: %.cc %.hip %.hip.hpp
	hipcc -o $@ $^ $(FLAGS) $(OPT) -fopenmp

clean:
	rm -f 1_sum/*.o 1_sum/*.hip.hpp 1_sum/*.hip 1_sum/reduce-cuda 1_sum/reduce-hip \
				2_stat/*.o 2_stat/*.hip.hpp 2_stat/*.hip 2_stat/reduce-cuda 2_stat/reduce-hip

.PHONY: clean hipify-commons
