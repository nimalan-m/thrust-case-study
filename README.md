# Thrust

## Building

**Building Source**

```
make all

# Compile with OpenMP Offloading(supported compiler needed)

make OPT=-DOFFLOAD all

```

**Running the Benchmark and generating Stats**

```
host=Ryzen5800x threads=16  target=cuda device=ptx1650 ./scripts/bench.sh

host=EPYC7551 threads=32  target=cuda device=tesla-p40 ./scripts/bench.sh
```

## Experiments

1. **Sum reduction:** Baseline is computing sum with double, compared against single precision float

**Cases**
1. Golden - Sum on CPU with OpenMP. 
2. Thrust - Reduction with Thrust reduce. 
3. Cuda - Reduction with Cuda using warp intrinsics. 
4. OpenMP Offloading - Reduction with OpenMP offloaded to GPU. 

**Insights:**

1. Serial reduction suffers from low accuracy for high value of N because of precision loss
2. Parallelism reduces the loss observed
3. Thrust seems to have the least loss in accuracy
4. For both GPU implementation a warmup is done as the very first kernel launched in the application is slow
5. Thrust uses CUB internally

**Precision loss**

![Precision loss](./benchmark/precision_loss.png)  

**Tesla P40**

![Serial Stats](./benchmark/tesla-p40-EPYC7551-32/images/stat_1_sum.png)  

**Serial Run**

![Serial Stats](./benchmark/ptx1650-Ryzen5800x-1/images/stat_1_sum.png)  

**Parallel Run 16 threads**

![16threads Stats](./benchmark/ptx1650-Ryzen5800x-16/images/stat_1_sum.png)  

**AMD Radeon**

![Radeon](./benchmark/radeonvii-Ryzen3700x-16/images/stat_1_sum.png)  

**Raw cuda**

```
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   87.21%  32.899ms         1  32.899ms  32.899ms  32.899ms  [CUDA memcpy HtoD]
                   12.78%  4.8224ms         1  4.8224ms  4.8224ms  4.8224ms  void grid_reduce<float>(float*, float*, int)
                    0.00%  1.4080us         1  1.4080us  1.4080us  1.4080us  [CUDA memcpy DtoH]
      API calls:   72.49%  102.14ms         1  102.14ms  102.14ms  102.14ms  cudaStreamCreate
                   26.83%  37.797ms         2  18.898ms  4.8741ms  32.923ms  cudaMemcpyAsync
                    0.26%  360.38us         2  180.19us  51.167us  309.21us  cudaFree
                    0.20%  280.41us         2  140.20us  62.367us  218.04us  cudaMalloc
                    0.11%  159.54us       101  1.5790us      80ns  70.403us  cuDeviceGetAttribute
                    0.06%  86.884us         1  86.884us  86.884us  86.884us  cuDeviceTotalMem
                    0.03%  37.080us         1  37.080us  37.080us  37.080us  cudaLaunchKernel
                    0.02%  22.923us         1  22.923us  22.923us  22.923us  cuDeviceGetName
                    0.00%  4.4680us         1  4.4680us  4.4680us  4.4680us  cuDeviceGetPCIBusId
                    0.00%  2.7850us         1  2.7850us  2.7850us  2.7850us  cudaStreamSynchronize
                    0.00%  1.1030us         3     367ns     130ns     662ns  cuDeviceGetCount
                    0.00%     732ns         2     366ns     100ns     632ns  cuDeviceGet
                    0.00%     161ns         1     161ns     161ns     161ns  cuDeviceGetUuid

```

**Thrust**

```
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   87.68%  33.001ms         1  33.001ms  33.001ms  33.001ms  [CUDA memcpy HtoD]
                    6.26%  2.3552ms         1  2.3552ms  2.3552ms  2.3552ms  void thrust::cuda_cub::core::_kernel_agent<thrust::cuda_cub::__parallel_for::ParallelForAgent<thrust::cuda_cub::__uninitialized_fill::functor<thrust::device_ptr<float>, float>, unsigned long>, thrust::cuda_cub::__uninitialized_fill::functor<thrust::device_ptr<float>, float>, unsigned long>(thrust::device_ptr<float>, float)
                    6.05%  2.2766ms         1  2.2766ms  2.2766ms  2.2766ms  void cub::DeviceReduceKernel<cub::DeviceReducePolicy<float, float, int, thrust::plus<float>>::Policy600, thrust::detail::normal_iterator<thrust::device_ptr<float>>, float*, int, thrust::plus<float>>(float, int, float, cub::GridEvenShare<int>, thrust::plus<float>)
                    0.01%  3.2320us         1  3.2320us  3.2320us  3.2320us  void cub::DeviceReduceSingleTileKernel<cub::DeviceReducePolicy<float, float, int, thrust::plus<float>>::Policy600, float*, float*, int, thrust::plus<float>, float>(float, int, float, thrust::plus<float>, cub::DeviceReducePolicy<float, float, int, thrust::plus<float>>::Policy600)
                    0.00%  1.3760us         1  1.3760us  1.3760us  1.3760us  [CUDA memcpy DtoH]
      API calls:   86.96%  255.80ms         2  127.90ms  109.56us  255.69ms  cudaMalloc
                   11.23%  33.030ms         2  16.515ms  14.707us  33.015ms  cudaMemcpyAsync
                    1.60%  4.7106ms         4  1.1777ms     611ns  2.3560ms  cudaStreamSynchronize
                    0.10%  300.97us         2  150.48us  73.729us  227.24us  cudaFree
                    0.05%  145.09us       101  1.4360us      61ns  65.023us  cuDeviceGetAttribute
                    0.03%  75.162us         1  75.162us  75.162us  75.162us  cuDeviceTotalMem
                    0.01%  40.055us         3  13.351us  4.9390us  20.038us  cudaLaunchKernel
                    0.01%  20.098us         1  20.098us  20.098us  20.098us  cuDeviceGetName
                    0.00%  8.3250us         1  8.3250us  8.3250us  8.3250us  cudaFuncGetAttributes
                    0.00%  4.7090us         1  4.7090us  4.7090us  4.7090us  cuDeviceGetPCIBusId
                    0.00%  3.9780us         7     568ns     220ns  1.5930us  cudaGetDevice
                    0.00%  3.9250us        38     103ns      60ns     531ns  cudaGetLastError
                    0.00%  2.4150us         2  1.2070us     701ns  1.7140us  cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
                    0.00%  1.8530us         3     617ns     310ns  1.0620us  cudaDeviceGetAttribute
                    0.00%  1.5220us         3     507ns     130ns  1.1620us  cuDeviceGetCount
                    0.00%     561ns         2     280ns     100ns     461ns  cuDeviceGet
                    0.00%     542ns         6      90ns      60ns     131ns  cudaPeekAtLastError
                    0.00%     190ns         1     190ns     190ns     190ns  cudaGetDeviceCount
                    0.00%     120ns         1     120ns     120ns     120ns  cuDeviceGetUuid

```

**OpenMP Offloading:**
```
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   67.09%  65.018ms         4  16.254ms     800ns  33.049ms  [CUDA memcpy HtoD]
                   32.90%  31.887ms         2  15.943ms  15.941ms  15.945ms  _Z27sum_reduce_bench_offloadingIfET_PKS0_m$_omp_fn$0
                    0.00%  3.0720us         2  1.5360us  1.5360us  1.5360us  [CUDA memcpy DtoH]
      API calls:   44.10%  104.12ms         1  104.12ms  104.12ms  104.12ms  cuCtxCreate
                   27.61%  65.200ms         4  16.300ms  72.079us  33.068ms  cuMemcpyHtoD
                   13.53%  31.947ms         2  15.973ms  15.944ms  16.002ms  cuCtxSynchronize
                   12.35%  29.158ms         1  29.158ms  29.158ms  29.158ms  cuCtxDestroy
                    0.74%  1.7426ms        25  69.703us  14.739us  748.78us  cuLinkAddData
                    0.70%  1.6611ms         1  1.6611ms  1.6611ms  1.6611ms  cuModuleLoadData
                    0.34%  805.68us         3  268.56us  175.83us  427.00us  cuMemAlloc
                    0.33%  789.00us         3  263.00us  220.16us  294.14us  cuMemFree
                    0.17%  396.63us         1  396.63us  396.63us  396.63us  cuLinkComplete
                    0.06%  132.59us         2  66.293us  19.888us  112.70us  cuLaunchKernel
                    0.03%  72.351us        16  4.5210us      70ns  69.624us  cuDeviceGetAttribute
                    0.01%  28.966us         2  14.483us  14.388us  14.578us  cuMemcpyDtoH
                    0.01%  19.568us         1  19.568us  19.568us  19.568us  cuLinkCreate
                    0.00%  11.783us         1  11.783us  11.783us  11.783us  cuDeviceGetName
                    0.00%  7.3260us        13     563ns     100ns  2.4650us  cuCtxGetDevice
                    0.00%  7.1840us         8     898ns     150ns  2.5150us  cuMemGetAddressRange
                    0.00%  4.6190us         1  4.6190us  4.6190us  4.6190us  cuDeviceGetPCIBusId
                    0.00%     962ns         1     962ns     962ns     962ns  cuLinkDestroy
                    0.00%     841ns         4     210ns      80ns     441ns  cuDeviceGetCount
                    0.00%     631ns         1     631ns     631ns     631ns  cuModuleGetFunction
                    0.00%     612ns         2     306ns     241ns     371ns  cuFuncGetAttribute
                    0.00%     551ns         2     275ns      70ns     481ns  cuDeviceGet
                    0.00%     430ns         1     430ns     430ns     430ns  cuInit
                    0.00%     421ns         1     421ns     421ns     421ns  cuModuleGetGlobal
                    0.00%     251ns         1     251ns     251ns     251ns  cuCtxGetCurrent
                    0.00%      91ns         1      91ns      91ns      91ns  cuDriverGetVersion
```

---

2. **Stats Reduction:** Calculating mean, min, max of array. Baseline with double compared against single precision float. GPU implementation supports only single precision floats although there are templates

**Cases**
1. Golden - Sum on CPU with OpenMP
2. Thrust - Reduction with Thrust transform_reduce
3. Cuda - Reduction with Cuda using warp intrinsics
4. OpenMP Offloading - Reduction with OpenMP offloaded to GPU

**Tesla P40**

![Serial Stats](./benchmark/tesla-p40-EPYC7551-32/images/stat_2_stat.png)  

**Serial Run**

![Serial Stats](./benchmark/ptx1650-Ryzen5800x-1/images/stat_2_stat.png)  

**Parallel Run(16threads)**

![16threads Stats](./benchmark/ptx1650-Ryzen5800x-16/images/stat_2_stat.png)  

**AMD Radeon**

![Radeon](./benchmark/radeonvii-Ryzen3700x-16/images/stat_2_stat.png)

**Insights:**

1. OpenMP based CPU version runs into accuracy loss for mean at high N
2. Cuda and Thrust version perform identically, although Thrust involves custom functors to calculate all the stats in a single pass
3. Raw wallclock time without data transfer was measured, Thrust seems to outperform the Cuda implementation
4. The CUDA implementation can be profiled and tuned to improve performance, the take away for this experiment is that a basic Thrust implementation can provide very good performance without tuning
5. Looking at the nvprof report of Thrust we see CUB being used which can be attributed for the performance
6. OpenMP Offloading is the simplest of all the GPU approaches, although it trails behind Thrust and CUDA

**Thrust**
Results from nvprof

```
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   86.08%  131.92ms         2  65.961ms  65.923ms  66.000ms  [CUDA memcpy HtoD]
                    7.13%  10.934ms         2  5.4668ms  5.1686ms  5.7649ms  void cub::DeviceReduceKernel<cub::DeviceReducePolicy<stats_data, stats_data, int, stats_data_binary_op>::Policy600, thrust::cuda_cub::transform_input_iterator_t<stats_data, thrust::detail::normal_iterator<thrust::device_ptr<float>>, stat_data_unary_op>, stats_data*, int, stats_data_binary_op>(stats_data, int, stats_data_binary_op, cub::GridEvenShare<int>, cub::DeviceReducePolicy<stats_data, stats_data, int, stats_data_binary_op>::Policy600)
                    6.77%  10.379ms         2  5.1895ms  4.6278ms  5.7512ms  void thrust::cuda_cub::core::_kernel_agent<thrust::cuda_cub::__parallel_for::ParallelForAgent<thrust::cuda_cub::__uninitialized_fill::functor<thrust::device_ptr<float>, float>, unsigned long>, thrust::cuda_cub::__uninitialized_fill::functor<thrust::device_ptr<float>, float>, unsigned long>(thrust::device_ptr<float>, float)
                    0.01%  14.594us         2  7.2970us  7.1690us  7.4250us  void cub::DeviceReduceSingleTileKernel<cub::DeviceReducePolicy<stats_data, stats_data, int, stats_data_binary_op>::Policy600, stats_data*, stats_data*, int, stats_data_binary_op, stats_data>(stats_data, int, stats_data_binary_op, cub::DeviceReducePolicy<stats_data, stats_data, int, stats_data_binary_op>::Policy600, stats_data*)
                    0.00%  2.8160us         2  1.4080us  1.3760us  1.4400us  [CUDA memcpy DtoH]
      API calls:   82.60%  736.38ms         4  184.10ms  148.11us  735.64ms  cudaMalloc
                   14.80%  131.99ms         4  32.997ms  17.553us  66.010ms  cudaMemcpyAsync
                    2.43%  21.686ms         8  2.7108ms     711ns  5.8419ms  cudaStreamSynchronize
                    0.10%  900.81us         4  225.20us  69.629us  381.24us  cudaFree
                    0.03%  229.93us       101  2.2760us     140ns  102.68us  cuDeviceGetAttribute
                    0.02%  138.25us         1  138.25us  138.25us  138.25us  cuDeviceTotalMem
                    0.01%  108.41us         6  18.068us  5.5400us  27.311us  cudaLaunchKernel
                    0.00%  30.025us         1  30.025us  30.025us  30.025us  cuDeviceGetName
                    0.00%  17.072us        13  1.3130us     281ns  8.2160us  cudaGetDevice
                    0.00%  11.945us        73     163ns      70ns  1.4730us  cudaGetLastError
                    0.00%  6.7040us         6  1.1170us     331ns  1.8740us  cudaDeviceGetAttribute
                    0.00%  5.8310us         4  1.4570us     731ns  2.1240us  cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
                    0.00%  5.1200us         1  5.1200us  5.1200us  5.1200us  cudaFuncGetAttributes
                    0.00%  3.2760us         1  3.2760us  3.2760us  3.2760us  cuDeviceGetPCIBusId
                    0.00%  1.7730us         3     591ns     220ns  1.0320us  cuDeviceGetCount
                    0.00%  1.4120us        12     117ns      80ns     241ns  cudaPeekAtLastError
                    0.00%  1.1220us         2     561ns     200ns     922ns  cuDeviceGet
                    0.00%     310ns         1     310ns     310ns     310ns  cuDeviceGetUuid
                    0.00%     270ns         1     270ns     270ns     270ns  cudaGetDeviceCount
```
