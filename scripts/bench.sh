#!/usr/bin/env bash

set -e
set -x

declare -a ALL=("1_sum" "2_stat")
#declare -a ALL=("2_stat")
declare -a SIZE=("1000000" "2000000" "5000000" "8000000" \
                "10000000" "20000000" "50000000" "80000000" \
                "100000000" "200000000" "500000000")

rm -f benchmark/$device-$host-$threads/stat*.txt
rm -f benchmark/$device-$host-$threads/stat*.csv

mkdir -p benchmark/$device-$host-$threads
mkdir -p benchmark/$device-$host-$threads/images

for bench in "${ALL[@]}"
do
  for size in "${SIZE[@]}"
  do
    echo "$bench/reduce-$target $size" &>> "benchmark/$device-$host-$threads/stat_$bench.txt"
    $bench/reduce-$target $size &>> "benchmark/$device-$host-$threads/stat_$bench.txt"

  done

      cat "benchmark/$device-$host-$threads/stat_$bench.txt" | awk '          \
      BEGIN {                     \
        FS = " ";                 \
      }                           \
      /reduce/ {                  \
        N = $NF;                  \
      }                           \
      /Elapsed time golden/ {     \
        split($NF, time, "us");   \
        golden=time[1];           \
      }                           \
      /Elapsed time thrust/ {     \
        split($NF, time, "us");   \
        thrust=time[1];           \
      }                           \
      /Elapsed time offloading/ { \
        split($NF, time, "us");   \
        offloading=time[1];       \
      }                           \
      /Elapsed time cuda/ {       \
        split($NF, time, "us");   \
        cuda=time[1];             \
        printf("%s, %s, %s, %s, %s\n",          \
        N, golden, thrust, cuda, offloading); \
      }                           \
    ' > "benchmark/$device-$host-$threads/stat_$bench.csv"

    cat "benchmark/$device-$host-$threads/stat_$bench.txt" | awk '          \
      BEGIN {                     \
        FS = " ";                 \
      }                           \
      /reduce/ {                  \
        N = $NF;                  \
        printf("\n");              \
        printf("%s ", N);         \
      }                           \
      /Deviation from double precision/ {     \
        split($NF, prec, "%");   \
        printf("%s ", prec[1]);        \
      }                           \
    ' > "benchmark/$device-$host-$threads/stat_precision_$bench.csv"

  echo "
    reset; \
    set terminal png enhanced large font \"Helvetica,10\";     \
    # set multiplot layout 2,1;             \

    set title \" Benchmark Runtime \";   \
    set xlabel \"Array Size\";              \
    set ylabel \"Execution time (us)\";     \
    set key left top;                       \
    set logscale x;                         \
    set logscale y;                         \

    plot  \"benchmark/$device-$host-$threads/stat_$bench.csv\" using 1:(\$2/1) with linespoint title \"CPU OpenMP $host Threads: $threads\", \
          \"benchmark/$device-$host-$threads/stat_$bench.csv\" using 1:(\$3/1) with linespoint title \"Thrust $device\", \
          \"benchmark/$device-$host-$threads/stat_$bench.csv\" using 1:(\$4/1) with linespoint title \"$target $device\"; \
    " | gnuplot > "benchmark/$device-$host-$threads/images/stat_$bench.png"
done

#\"benchmark/$device-$host-$threads/stat_$bench.csv\" using 1:(\$5/1) with linespoint title \"OpenMP Offloading\", \

# set title \" Accuracy compared to double precision \";   \
# set xlabel \"Array Size\";           \
# set ylabel \"Accuracy loss %\";       \
# set key left top;                     \
# set logscale x;                       \
# plot  \"benchmark/$device-$host-$threads/stat_precision_$bench.csv\" using 1:(\$2/1) with linespoint title \"CPU OpenMP\", \
#       \"benchmark/$device-$host-$threads/stat_precision_$bench.csv\" using 1:(\$3/1) with linespoint title \"OpenMP Offloading\", \
#       \"benchmark/$device-$host-$threads/stat_precision_$bench.csv\" using 1:(\$4/1) with linespoint title \"Thrust\", \
#       \"benchmark/$device-$host-$threads/stat_precision_$bench.csv\" using 1:(\$5/1) with linespoint title \"$target\"; \



