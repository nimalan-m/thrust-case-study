#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <limits>
#include <vector>

#include "helpers.hh"
#include "reduce.cuh"

template <typename T>
void sum_reduce_golden(const T *arr, const size_t arrSize, T &mean, T &min,
                       T &max) {

#pragma omp parallel for reduction(+ : mean) reduction(max : max) reduction(min : min)
  for (size_t idx = 0; idx < arrSize; idx++) {
    mean += arr[idx];
    min = std::min(min, arr[idx]);
    max = std::max(max, arr[idx]);
  }
  mean /= (T)arrSize;
}

template <typename T>
void bench_golden(std::vector<T> arr, size_t N, T &mean, T &min, T &max,
                  bool warmup) {
  (void)N;
  WallClock t;
  t.reset();

  sum_reduce_golden<T>(arr.data(), arr.size(), mean, min, max);
  double timeTaken = t.elapsedTimeUS();
  if (!warmup) {
    std::cout << "Elapsed time golden: " << timeTaken << "us" << std::endl;
  }
}

#ifdef OFFLOAD
template <typename T>
void sum_reduce_bench_offloading(const T *arr, const size_t arrSize, T &mean,
                                 T &min, T &max) {

#pragma omp target map(to : arr[:arrSize], arrSize) map(tofrom : mean, min, max)
  {
#pragma omp teams distribute parallel for reduction(+ : mean) reduction(min : min) reduction(max : max)
    for (size_t idx = 0; idx < arrSize; idx++) {
      mean += arr[idx];
      min = std::min(min, arr[idx]);
      max = std::max(max, arr[idx]);
    }
  }
  mean /= (T)arrSize;
}

template <typename T>
void bench_offloading(std::vector<T> arr, size_t N, T &mean, T &min, T &max,
                      bool warmup) {
  (void)N;
  WallClock t;
  t.reset();
  sum_reduce_bench_offloading<T>(arr.data(), arr.size(), mean, min, max);
  if (!warmup)
    std::cout << "Elapsed time offloading: " << t.elapsedTimeUS() << "us"
              << std::endl;
}
#endif

template <typename T> void bench_sum_reduce(size_t N) {
  std::srand(time(0));
  // Array of function pointers
  void (*bench_cases[])(std::vector<T>, size_t, T &, T &, T &,
                        bool) = {bench_golden, bench_thrust,
#ifdef OFFLOAD
                                 bench_offloading,
#endif
                                 bench_cuda};
  size_t cases = sizeof(bench_cases) /
                 sizeof(void (*)(std::vector<T>, size_t, T, T, T, bool));

  std::vector<T> arr(N);
  T range = 100.0f;
  T offset = 10.0f;

  std::generate(arr.begin(), arr.end(), [offset, range]() -> T {
    return offset + (rand() / (T)RAND_MAX) * range;
  });

#ifdef ASSERT
  std::vector<double> arr_dbl(N);
  for (size_t idx = 0; idx < N; idx++)
    arr_dbl[idx] = (double)arr[idx];

  double expected_mean = 0.0;
  double expected_min = std::numeric_limits<double>::max();
  double expected_max = std::numeric_limits<double>::min();

  // Assert with double to compensate for precision loss
  sum_reduce_golden<double>(arr_dbl.data(), N, expected_mean, expected_min,
                            expected_max);

  std::cout << "Mean: " << expected_mean << std::endl;
  std::cout << "Min: " << expected_min << std::endl;
  std::cout << "Max: " << expected_max << std::endl;

#endif

  // Warmup
  for (size_t idx = 1; idx < cases; idx++) {
    T mean;
    T min;
    T max;
    bench_cases[idx](arr, N, mean, min, max, true);
  }

  // Actual run
  for (size_t idx = 0; idx < cases; idx++) {
    T actual_mean = 0.0;
    T actual_min = std::numeric_limits<T>::max();
    T actual_max = std::numeric_limits<T>::min();
    bench_cases[idx](arr, N, actual_mean, actual_min, actual_max, false);

#ifdef ASSERT
    assert_fp(expected_mean, actual_mean, "Mean");
    assert_fp(expected_min, actual_min, "min");
    assert_fp(expected_max, actual_max, "max");
#endif
    std::cout << std::endl;
  }
}

int main(int argc, char **argv) {
  if (argc < 2) {
    std::cerr << "Usage: reduce <N>" << std::endl;
    return 1;
  }
  std::cout.precision(5);

  int N = std::atoi(argv[1]);

  bench_sum_reduce<float>(N);
}
