#pragma once

#include <cstddef>
#include <vector>

template <typename T>
void bench_thrust(std::vector<T> arr, size_t N, T &mean, T &min, T &max, bool warmup);

template <typename T>
void bench_cuda(std::vector<T> arr, size_t N, T &mean, T &min, T &max, bool warmup);
