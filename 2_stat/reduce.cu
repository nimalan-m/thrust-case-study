#include <algorithm>
#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <cfloat>
#include <limits>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/transform_reduce.h>
#include <thrust/functional.h>

#ifdef __HIP_PLATFORM_HCC__
#include "helpers-cuda.hip.hpp"
#else
#include "helpers-cuda.cuh"
#endif
#include "helpers.hh"

#include "helpers.hh"

template <typename T>
__inline__ __device__ void wrap_reduce_stat(T &sum, T &min, T &max) {
  for (uint i = warpSize / 2; i >= 1; i >>= 1) {
    sum += SHFL_DOWN(sum, i);
    min = fminf(min, SHFL_DOWN(min, i));
    max = fmaxf(max, SHFL_DOWN(max, i));
  }
}

template <typename T>
__inline__ __device__ void thread_block_reduce(T &sum, T &min, T &max) {
  static __shared__ T shared_sum[64];
  static __shared__ T shared_min[64];
  static __shared__ T shared_max[64];
  uint tid = threadIdx.x;

  uint lane = tid % warpSize;
  uint warpid = tid / warpSize;

  wrap_reduce_stat(sum, min, max);

  // Result of reduction in lane 0
  if (lane == 0) {
    shared_sum[warpid] = sum;
    shared_min[warpid] = min;
    shared_max[warpid] = max;
  }

  __syncthreads();

  sum = (tid < blockDim.x / warpSize) ? shared_sum[lane] : 0;
  min = (tid < blockDim.x / warpSize) ? shared_min[lane] : FLT_MAX;
  max = (tid < blockDim.x / warpSize) ? shared_max[lane] : FLT_MIN;

  if (warpid == 0) {
    wrap_reduce_stat(sum, min, max);
  }
}

template <typename T>
__global__ void grid_reduce(T *in, T *out_sum, T *out_min, T *out_max, int N) {
  T sum = 0;
  T min = FLT_MAX;
  T max = FLT_MIN;

  for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < N;
       i += blockDim.x * gridDim.x) {
    sum += in[i];
    min = fminf(min, in[i]);
    max = fmaxf(max, in[i]);
  }
  thread_block_reduce(sum, min, max);
  if (threadIdx.x == 0) {
    atomicAdd(out_sum, sum);
    // atomicMin not available for float
    out_min[blockIdx.x] = min;
    out_max[blockIdx.x] = max;
  }
}

template <typename T>
double sum_reduce_cuda(const T *arr, const size_t arrSize, T &mean, T &min,
                       T &max) {
  const int blocks = 64;
  T out_sum;
  T out_min[blocks];
  T out_max[blocks];

  cudaStream_t stream;
  cudaEvent_t start, stop;

  checkGPUErrors(cudaStreamCreate(&stream));
  checkGPUErrors(cudaEventCreate(&start));
  checkGPUErrors(cudaEventCreate(&stop));

  T *arr_d;
  T *out_sum_d, *out_min_d, *out_max_d;
  checkGPUErrors(cudaMalloc(&arr_d, sizeof(T) * arrSize));
  checkGPUErrors(cudaMalloc(&out_sum_d, sizeof(T) * 1));
  checkGPUErrors(cudaMalloc(&out_min_d, sizeof(T) * blocks));
  checkGPUErrors(cudaMalloc(&out_max_d, sizeof(T) * blocks));

  checkGPUErrors(cudaMemcpyAsync(arr_d, arr, sizeof(T) * arrSize,
                                 cudaMemcpyHostToDevice, stream));

  checkGPUErrors(cudaEventRecord(start, stream));

  grid_reduce<<<blocks, 256, 0, stream>>>(arr_d, out_sum_d, out_min_d,
                                          out_max_d, arrSize);

  checkGPUErrors(cudaMemcpyAsync(&out_sum, out_sum_d, sizeof(T) * 1,
                                 cudaMemcpyDeviceToHost, stream));
  checkGPUErrors(cudaMemcpyAsync(out_min, out_min_d, sizeof(T) * blocks,
                                 cudaMemcpyDeviceToHost, stream));
  checkGPUErrors(cudaMemcpyAsync(out_max, out_max_d, sizeof(T) * blocks,
                                 cudaMemcpyDeviceToHost, stream));

  checkGPUErrors(cudaStreamSynchronize(stream));
  checkGPUErrors(cudaEventRecord(stop, stream));
  checkGPUErrors(cudaEventSynchronize(stop));

  float elapsedTime;
  checkGPUErrors(cudaEventElapsedTime(&elapsedTime, start, stop));
  getLastGPUError("Error running kernel");

  checkGPUErrors(cudaFree(arr_d));
  checkGPUErrors(cudaFree(out_sum_d));
  checkGPUErrors(cudaFree(out_min_d));
  checkGPUErrors(cudaFree(out_max_d));

  mean = out_sum;
  min = std::numeric_limits<T>::max();
  max = std::numeric_limits<T>::min();
  for (int i = 0; i < blocks; i++) {
    min = std::min(min, out_min[i]);
    max = std::max(max, out_max[i]);
  }
  mean /= (T)arrSize;

  return elapsedTime;
}

template <typename T>
void bench_cuda(std::vector<T> arr, size_t N, T &mean, T &min, T &max,
                bool warmup) {
  (void)N;
  WallClock t;
  t.reset();
  double elapsedTimeWithoutTransfer =
      sum_reduce_cuda<T>(arr.data(), arr.size(), mean, min, max);
  if (!warmup) {
    std::cout << "Elapsed time without transfer: " << elapsedTimeWithoutTransfer
              << "ms" << std::endl;
    std::cout << "Elapsed time cuda: " << t.elapsedTimeUS() << "us"
              << std::endl;
  }
}

template void bench_cuda(std::vector<float> arr, size_t N, float &mean,
                         float &min, float &max, bool warmup);

struct stats_data {
  int count;
  float mean;
  float min;
  float max;
  // float m2;

  void init() {
    count = 0;
    // mean = min = max = m2 = 0.0;
    mean = min = max = 0.0;
    min = FLT_MAX;
    max = FLT_MIN;
  }

  // float variance() { return m2 / (float)count; }
};

struct stat_data_unary_op {
  __host__ __device__ stats_data operator()(const float &x) const {
    stats_data res;
    if (isnan(x)) {
      res.count = 0;
      res.min = FLT_MAX;
      res.max = FLT_MIN;
      res.mean = 0;
      // res.m2 = 0;
    } else {
      res.count = 1;
      res.min = x;
      res.max = x;
      res.mean = x;
      // res.m2 = 0;
    }
    return res;
  }
};

struct stats_data_binary_op
    : public thrust::binary_function<const stats_data &, const stats_data &,
                                     stats_data> {
  __host__ __device__ stats_data operator()(const stats_data &x,
                                            const stats_data &y) {
    stats_data res;

    if (x.count == 0) {
      res.count = y.count;
      res.min = y.min;
      res.max = y.max;
      res.mean = y.mean;
      // res.m2 = y.m2;
    } else if (y.count == 0) {
      res.count = x.count;
      res.min = x.min;
      res.max = x.max;
      res.mean = x.mean;
      // res.m2 = x.m2;
    } else {
      int count = x.count + y.count;
      float delta = y.mean - x.mean;
      // float delta2 = delta * delta;

      res.count = count;
      res.min = thrust::min(x.min, y.min);
      res.max = thrust::max(x.max, y.max);
      res.mean = x.mean + delta * y.count / count;

      // res.m2 = x.m2 + y.m2;
      // res.m2 += delta2 * x.count * y.count / count;
    }

    return res;
  }
};

template <typename T>
double sum_reduce_thrust(const std::vector<T> &arr, T &mean, T &min, T &max) {
  thrust::device_vector<T> arr_d(arr.size());

  arr_d = arr;

  stat_data_unary_op unary_op;
  stats_data_binary_op binary_op;
  stats_data init;

  init.init();

  WallClock t1;

  stats_data res = thrust::transform_reduce(arr_d.begin(), arr_d.end(),
                                            unary_op, init, binary_op);

  double timeTakenWithoutTransfer = t1.elapsedTimeMS();

  mean = res.mean;
  min = res.min;
  max = res.max;

  return timeTakenWithoutTransfer;
}

template <typename T>
void bench_thrust(std::vector<T> arr, size_t N, T &mean, T &min, T &max,
                  bool warmup) {
  (void)N;
  WallClock t;
  t.reset();
  double elapsedTimeWithoutTransfer = sum_reduce_thrust<T>(arr, mean, min, max);
  if (!warmup) {
    std::cout << "Elapsed time without transfer: " << elapsedTimeWithoutTransfer
              << "ms" << std::endl;
    std::cout << "Elapsed time thrust: " << t.elapsedTimeUS() << "us"
              << std::endl;
  }
}
template void bench_thrust(std::vector<float> arr, size_t N, float &mean,
                           float &min, float &max, bool warmup);
