#pragma once

#include <cstddef>
#include <vector>

template <typename T>
T bench_thrust(std::vector<T> arr, size_t N, bool warmup);

template <typename T>
T bench_cuda(std::vector<T> arr, size_t N, bool warmup);
