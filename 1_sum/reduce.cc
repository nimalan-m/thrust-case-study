#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

#include "helpers.hh"
#include "reduce.cuh"

template <typename T> T sum_reduce_golden(const T *arr, const size_t arrSize) {
  T sum = 0.0;

#pragma omp parallel for reduction(+ : sum)
  for (size_t idx = 0; idx < arrSize; idx++) {
    sum += arr[idx];
  }
  return sum;
}

template <typename T>
T bench_golden(std::vector<T> arr, size_t N, bool warmup) {
  (void)N;
  WallClock t;
  t.reset();
  T actual = sum_reduce_golden<T>(arr.data(), arr.size());
  if (!warmup)
    std::cout << "Elapsed time golden: " << t.elapsedTimeUS() << "us"
              << std::endl;

  return actual;
}

#ifdef OFFLOAD
template <typename T>
T sum_reduce_bench_offloading(const T *arr, const size_t arrSize) {
  T sum = 0.0;

#pragma omp target map(to : arr[:arrSize], arrSize) map(tofrom : sum)
  {
#pragma omp teams distribute parallel for reduction(+ : sum)
    for (size_t idx = 0; idx < arrSize; idx++) {
      sum += arr[idx];
    }
  }

  return sum;
}

template <typename T>
T bench_offloading(std::vector<T> arr, size_t N, bool warmup) {
  (void)N;
  WallClock t;
  t.reset();
  T actual = sum_reduce_bench_offloading<T>(arr.data(), arr.size());
  if (!warmup)
    std::cout << "Elapsed time offloading: " << t.elapsedTimeUS() << "us"
              << std::endl;

  return actual;
}
#endif

template <typename T> void bench_sum_reduce(size_t N) {

  // Array of function pointers
  T(*bench_cases[])
  (std::vector<T>, size_t, bool) = {bench_golden,
                                    bench_thrust,
                                    #ifdef OFFLOAD
                                    bench_offloading,
                                    #endif
                                    bench_cuda
                                    };
  size_t cases =
      sizeof(bench_cases) / sizeof(T(*)(std::vector<T>, size_t, bool));

  std::vector<T> arr(N);
  T range = 100.0f;
  T offset = 10.0f;

  std::generate(arr.begin(), arr.end(),
                [offset, range]() -> T { return offset + (rand() / (T)RAND_MAX) * range; });

#ifdef ASSERT
  std::vector<double> arr_dbl(N);
  for (size_t idx = 0; idx < N; idx++)
    arr_dbl[idx] = (double)arr[idx];

  // Assert with double to compensate for precision loss
  double expected = sum_reduce_golden<double>(arr_dbl.data(), N);
#endif

  // Warmup
  for (size_t idx = 1; idx < cases; idx++) {
    bench_cases[idx](arr, N, true);
  }

  // Actual run
  for (size_t idx = 0; idx < cases; idx++) {
    double actual = bench_cases[idx](arr, N, false);

#ifdef ASSERT
  assert_fp(expected, actual, "sum");
#endif
    std::cout << std::endl;
  }
}

int main(int argc, char **argv) {
  if (argc < 2) {
    std::cerr << "Usage: reduce <N>" << std::endl;
    return 1;
  }
  std::cout.precision(5);

  int N = std::atoi(argv[1]);

  bench_sum_reduce<float>(N);
}
