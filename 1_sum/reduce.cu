#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

#ifdef __HIP_PLATFORM_HCC__
#include "helpers-cuda.hip.hpp"
#else
#include "helpers-cuda.cuh"
#endif
#include "helpers.hh"

template <typename T> __inline__ __device__ T wrap_reduce_sum(T val) {
  for (uint i = warpSize / 2; i >= 1; i >>= 1)
    val += SHFL_DOWN(val, i);

  return val;
}

template <typename T> __inline__ __device__ T thread_block_reduce(T val) {
  static __shared__ T shared[64];
  uint tid = threadIdx.x;

  uint lane = tid % warpSize;
  uint warpid = tid / warpSize;

  val = wrap_reduce_sum(val);

  // Result of reduction in lane 0
  if (lane == 0) {
    shared[warpid] = val;
  }

  __syncthreads();

  val = (tid < blockDim.x / warpSize) ? shared[lane] : 0;

  if (warpid == 0) {
    val = wrap_reduce_sum(val);
  }

  return val;
}

template <typename T> __global__ void grid_reduce(T *in, T *out, int N) {
  T sum = 0;
  for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < N;
       i += blockDim.x * gridDim.x) {
    sum += in[i];
  }
  sum = thread_block_reduce(sum);
  if (threadIdx.x == 0) {
    atomicAdd(out, sum);
  }
}

template <typename T>
double sum_reduce_cuda(const T *arr, const size_t arrSize, T &sum) {
  const int blocks = 64;
  T out;

  cudaStream_t stream;
  cudaEvent_t start, stop;

  checkGPUErrors(cudaStreamCreate(&stream));
  checkGPUErrors(cudaEventCreate(&start));
  checkGPUErrors(cudaEventCreate(&stop));

  T *arr_d;
  T *out_d;
  checkGPUErrors(cudaMalloc(&arr_d, sizeof(T) * arrSize));
  checkGPUErrors(cudaMalloc(&out_d, sizeof(T) * 1));

  checkGPUErrors(cudaMemcpyAsync(arr_d, arr, sizeof(T) * arrSize,
                                 cudaMemcpyHostToDevice, stream));

  checkGPUErrors(cudaEventRecord(start, stream));

  grid_reduce<<<blocks, 256, 0, stream>>>(arr_d, out_d, arrSize);

  checkGPUErrors(cudaMemcpyAsync(&out, out_d, sizeof(T) * 1,
                                 cudaMemcpyDeviceToHost, stream));

  checkGPUErrors(cudaStreamSynchronize(stream));
  checkGPUErrors(cudaEventRecord(stop, stream));
  checkGPUErrors(cudaEventSynchronize(stop));

  float elapsedTime;
  checkGPUErrors(cudaEventElapsedTime(&elapsedTime, start, stop));
  getLastGPUError("Error running kernel");

  checkGPUErrors(cudaFree(arr_d));
  checkGPUErrors(cudaFree(out_d));

  sum = out;

  return elapsedTime;
}

template <typename T>
double sum_reduce_thrust(const std::vector<T> &arr, T &sum) {
  thrust::device_vector<T> arr_d(arr.size());

  arr_d = arr;

  WallClock t1;

  sum = thrust::reduce(arr_d.begin(), arr_d.end());

  return t1.elapsedTimeMS();
}

template <typename T>
T bench_thrust(std::vector<T> arr, size_t N, bool warmup) {
  (void)N;
  T actual = 0.0;

  WallClock t;
  t.reset();
  double elapsedTimeWithoutTransfer = sum_reduce_thrust<T>(arr, actual);
  if (!warmup) {
    std::cout << "Elapsed time without transfer: " << elapsedTimeWithoutTransfer
              << "ms" << std::endl;
    std::cout << "Elapsed time thrust: " << t.elapsedTimeUS() << "us"
              << std::endl;
  }

  return actual;
}
template float bench_thrust(std::vector<float> arr, size_t N, bool warmup);

template <typename T> T bench_cuda(std::vector<T> arr, size_t N, bool warmup) {
  (void)N;

  T actual = 0.0;

  WallClock t;
  t.reset();
  double elapsedTimeWithoutTransfer =
      sum_reduce_cuda<T>(arr.data(), arr.size(), actual);
  if (!warmup) {
    std::cout << "Elapsed time without transfer: " << elapsedTimeWithoutTransfer
              << "ms" << std::endl;
    std::cout << "Elapsed time cuda: " << t.elapsedTimeUS() << "us"
              << std::endl;
  }

  return actual;
}
template float bench_cuda(std::vector<float> arr, size_t N, bool warmup);
