#pragma once

// shfl_down is considered deprecated in Cuda
#ifdef __HIP_PLATFORM_HCC__
#define SHFL_DOWN(val, offset) __shfl_down(val, offset)
#else
#define SHFL_DOWN(val, offset) __shfl_down_sync(0xffffffff, val, offset)
#endif

inline void checkGPU(cudaError_t result, std::string file, int const line) {
  if (result != cudaSuccess) {
    fprintf(stderr, "Cuda Runtime Error at %s:%d : %s\n", file.c_str(), line,
            cudaGetErrorString(result));
    exit(EXIT_FAILURE);
  }
}

#define checkGPUErrors(val) checkGPU(val, __FILE__, __LINE__)

inline void __getLastGPUError(const char *errorMessage, const char *file,
                              const int line) {
  cudaError_t err = cudaGetLastError();

  if (cudaSuccess != err) {
    std::cerr << file << "(" << line << ")"
              << " : getLastCudaError() CUDA error : " << errorMessage << " : "
              << (int)err << " " << cudaGetErrorString(err) << "." << std::endl;
    exit(EXIT_FAILURE);
  }
}


#define getLastGPUError(msg) __getLastGPUError(msg, __FILE__, __LINE__)
