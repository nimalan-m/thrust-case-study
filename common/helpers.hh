#pragma once

#include <chrono>
#include <cmath>
#include <iostream>

#define EPSILON 0.1
#define ASSERT

class WallClock {
  std::chrono::steady_clock::time_point begin;

public:
  WallClock() { begin = std::chrono::steady_clock::now(); }
  void reset() { begin = std::chrono::steady_clock::now(); }
  double elapsedTimeUS() {
    std::chrono::steady_clock::time_point end =
        std::chrono::steady_clock::now();
    return std::chrono::duration_cast<std::chrono::microseconds>(end - begin)
        .count();
  }
  double elapsedTimeMS() {
    std::chrono::steady_clock::time_point end =
        std::chrono::steady_clock::now();
    return std::chrono::duration_cast<std::chrono::milliseconds>(end - begin)
        .count();
  }
};

inline void assert_fp(double expected, double actual, std::string case_name) {
  double diff = fabs(expected - actual);
  double accuracy_diff = (diff / expected) * 100.0;
  std::cout << "Deviation from double precision " << case_name << ": " << accuracy_diff << "%"
            << std::endl;
  // Percetage deviation
  if (accuracy_diff > EPSILON) {
    std::cerr << "Expected: " << expected << " Actual: " << actual << std::endl;
    // assert(accuracy_diff < EPSILON);
  }
}
